package com.testproduct.evgenynefedov.lastfm.di.scope;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

@Scope
@Retention(RUNTIME)
public @interface PerActivity {
};

