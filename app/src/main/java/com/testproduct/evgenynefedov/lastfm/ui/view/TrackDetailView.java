package com.testproduct.evgenynefedov.lastfm.ui.view;

import com.example.models.Image;
import com.example.models.Tag;

import java.util.Collection;
import java.util.List;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public interface TrackDetailView extends BaseView {

    void setListeners(String title);

    void setDescription(String description);

    void setAlbumAvatar(Image albumAvatar);
    void setPlayCount(String playCount);

    void renderTags(Collection<Tag> tagList);
}
