package com.testproduct.evgenynefedov.lastfm.di.module;

import android.content.Context;

import com.example.executors.PostExecutionThread;
import com.example.executors.ThreadExecutor;
import com.example.repository.TrackRepository;
import com.testproduct.evgenynefedov.data.cache.Cache;
import com.testproduct.evgenynefedov.data.cache.TrackDetailCache;
import com.testproduct.evgenynefedov.data.executor.JobExecutor;
import com.testproduct.evgenynefedov.data.repository.TrackDataRepository;
import com.testproduct.evgenynefedov.lastfm.jobs.UIThread;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class ApplicationModel {


    private Context appContext;

    public ApplicationModel(Context appContext) {
        this.appContext = appContext;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return appContext;
    }


    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @Singleton
    TrackRepository provideTrackRepository(TrackDataRepository trackDataRepository) {
        return trackDataRepository;
    }


    @Provides
    @Singleton
    Cache provideAnnouncementCache(TrackDetailCache trackCache) {
        return trackCache;
    }

}
