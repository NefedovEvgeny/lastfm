package com.testproduct.evgenynefedov.lastfm.ui.presenters;

import com.testproduct.evgenynefedov.lastfm.ui.view.TrackDetailView;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public interface TrackDetailPresenter extends Presenter<TrackDetailView> {
    void init(String trackId);
}
