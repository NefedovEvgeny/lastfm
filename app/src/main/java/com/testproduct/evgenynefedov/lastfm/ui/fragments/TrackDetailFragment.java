package com.testproduct.evgenynefedov.lastfm.ui.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.models.Image;
import com.example.models.Tag;
import com.example.models.TrackBase;
import com.pedrogomez.renderers.AdapteeCollection;
import com.pedrogomez.renderers.ListAdapteeCollection;
import com.pedrogomez.renderers.RVRendererAdapter;
import com.pedrogomez.renderers.Renderer;
import com.pedrogomez.renderers.RendererBuilder;
import com.testproduct.evgenynefedov.lastfm.R;
import com.testproduct.evgenynefedov.lastfm.di.components.DaggerTrackComponent;
import com.testproduct.evgenynefedov.lastfm.renders.TagsRender;
import com.testproduct.evgenynefedov.lastfm.renders.TrackFeedRender;
import com.testproduct.evgenynefedov.lastfm.ui.OnTagClickListener;
import com.testproduct.evgenynefedov.lastfm.ui.presenters.imp.TrackDetailPresenterImap;
import com.testproduct.evgenynefedov.lastfm.ui.view.TrackDetailView;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class TrackDetailFragment extends CoordinatorDetailFragment implements TrackDetailView, OnTagClickListener {

    private static final String TRACK_ID = "trackId";
    private String trackId;

    @Inject
    TrackDetailPresenterImap presenter;

    @Bind(R.id.rlTracks)
    RecyclerView rlTags;

    private RVRendererAdapter<Tag> adapter;

    public static TrackDetailFragment newInstance(String id) {

        Bundle args = new Bundle();
        args.putString(TRACK_ID, id);
        TrackDetailFragment fragment = new TrackDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setHasOptionsMenu(true);
        trackId = getArguments().getString(TRACK_ID, null);
    }


    @Override
    View onCreateContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_track_feed, container, false);
        ButterKnife.bind(this, root);
        initInjector();
        presenter.bind(this);
        init();
        return root;
    }

    private void init() {
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        rlTags.setLayoutManager(manager);
        rlTags.setHasFixedSize(true);
        adapter = initAdapter();
        rlTags.setAdapter(adapter);
        loadDetail(trackId);

    }

    void loadDetail(String trackId) {
        presenter.init(trackId);
    }


    private RVRendererAdapter<Tag> initAdapter() {
        final AdapteeCollection<Tag> videoCollection =
                new ListAdapteeCollection<>();
        Renderer<Tag> renderer = new TagsRender(this);
        RendererBuilder<Tag> rendererBuilder = new RendererBuilder<>(renderer);
        return new RVRendererAdapter<>(rendererBuilder, videoCollection);
    }


    @Override
    protected String getActionBarTitle(Resources resources) {
        return resources.getString(R.string.app_name);
    }

    private void initInjector() {
        DaggerTrackComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build().inject(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.destroy();
        //TODO  обновить и удерживать ссылку на очистку :)
        ButterKnife.unbind(this);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void setDescription(String description) {

    }

    @Override
    public void setAlbumAvatar(Image albumAvatar) {
        super.setAvatar(albumAvatar);
    }

    @Override
    public void renderTags(Collection<Tag> tagList) {
        adapter.clear();
        ;
        adapter.addAll(tagList);
        adapter.notifyDataSetChanged();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            getActivity().finish();
            return true;
        }
        return false;
    }

    @Override
    public void onTagClick(Tag tag) {

    }
}
