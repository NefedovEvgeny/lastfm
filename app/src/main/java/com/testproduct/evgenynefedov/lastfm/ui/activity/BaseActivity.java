package com.testproduct.evgenynefedov.lastfm.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.testproduct.evgenynefedov.lastfm.MainApplication;
import com.testproduct.evgenynefedov.lastfm.di.components.ApplicationComponent;
import com.testproduct.evgenynefedov.lastfm.di.module.ActivityModule;
import com.testproduct.evgenynefedov.lastfm.navigation.Navigator;

import javax.inject.Inject;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class BaseActivity extends AppCompatActivity {

    @Inject
    Navigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getApplicationComponent().inject(this);
    }



    protected void addFragment(int containerViewId, Fragment fragment) {
        final FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, fragment);
        fragmentTransaction.commit();
    }


    protected ApplicationComponent getApplicationComponent() {
        return ((MainApplication) getApplication()).getApplicationComponent();
    }

    protected ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }
}
