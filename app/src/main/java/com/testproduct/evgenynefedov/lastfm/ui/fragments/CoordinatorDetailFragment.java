package com.testproduct.evgenynefedov.lastfm.ui.fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.models.Image;
import com.squareup.picasso.Picasso;
import com.testproduct.evgenynefedov.lastfm.R;

public abstract class CoordinatorDetailFragment extends BaseFragment {
    protected Toolbar toolbar;

    TextView tvListeners;
    TextView tvPayCount;
    ImageView ivAvatar;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.toolbar_track_detail, container, false);

        FrameLayout frameLayout = (FrameLayout) root.findViewById(R.id.fragmentContainer);
        tvListeners = (TextView) root.findViewById(R.id.tvListeners);
        tvPayCount = (TextView) root.findViewById(R.id.tvPayCount);
        View content = onCreateContentView(inflater, container, savedInstanceState);
        ivAvatar = (ImageView) root.findViewById(R.id.ivAvatar);
        frameLayout.addView(content);
        toolbar = (Toolbar) root.findViewById(R.id.toolbar);
        return root;
    }

    public void setTitle(String title) {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        assert actionBar != null;
        actionBar.setTitle(title);
    }

    public void setPlayCount(String playCount) {
        tvPayCount.setText(playCount);
    }

    public void setListeners(String description) {
        tvListeners.setText(description);
    }

    public void setAvatar(Image avatar) {
        Picasso.with(getActivity()).load(avatar.getLink()).into(ivAvatar);
    }

    abstract View onCreateContentView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        String title = getActionBarTitle(getResources());
        String subTitle = getActionBarSubTitle(getResources());
        actionBar.setTitle(title);
        actionBar.setSubtitle(subTitle);

    }



    protected abstract String getActionBarTitle(Resources resources);

    protected String getActionBarSubTitle(Resources resources) {
        return null;
    }


}