package com.testproduct.evgenynefedov.lastfm;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.testproduct.evgenynefedov.lastfm.di.components.ApplicationComponent;
import com.testproduct.evgenynefedov.lastfm.di.components.DaggerApplicationComponent;
import com.testproduct.evgenynefedov.lastfm.di.module.ApplicationModel;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class MainApplication extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerApplicationComponent.builder()
                .applicationModel(new ApplicationModel(this))
                .build();
        component.inject(this);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }

    public static ApplicationComponent getComponent(Context context) {
        return ((MainApplication) context.getApplicationContext()).component;
    }

    public ApplicationComponent getApplicationComponent() {
        return this.component;
    }
}
