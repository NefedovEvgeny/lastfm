package com.testproduct.evgenynefedov.lastfm.ui.presenters;

import com.testproduct.evgenynefedov.lastfm.ui.view.BaseView;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public interface Presenter<T extends BaseView> {

    void resume();

    void pause();

    void destroy();

    void bind(T view);
}
