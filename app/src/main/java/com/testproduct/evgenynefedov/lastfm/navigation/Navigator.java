package com.testproduct.evgenynefedov.lastfm.navigation;

import android.content.Context;
import android.content.Intent;

import com.example.models.TrackBase;
import com.testproduct.evgenynefedov.lastfm.ui.activity.MainActivity;
import com.testproduct.evgenynefedov.lastfm.ui.activity.TrackDetailActivity;

import javax.inject.Inject;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class Navigator {
    @Inject
    public Navigator() {
    }

    public void navigateToTrackDetail(Context context, TrackBase trackBase) {
        context.startActivity(TrackDetailActivity.getLaunchIntent(context, trackBase.getMbid()));
    }
}
