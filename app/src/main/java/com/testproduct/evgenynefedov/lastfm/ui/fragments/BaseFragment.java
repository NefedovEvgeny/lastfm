package com.testproduct.evgenynefedov.lastfm.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.example.exception.DefaultErrorBundle;
import com.roger.catloadinglibrary.CatLoadingView;
import com.testproduct.evgenynefedov.lastfm.MainApplication;
import com.testproduct.evgenynefedov.lastfm.di.HasComponent;
import com.testproduct.evgenynefedov.lastfm.di.components.ApplicationComponent;
import com.testproduct.evgenynefedov.lastfm.di.module.ActivityModule;
import com.testproduct.evgenynefedov.lastfm.ui.view.BaseView;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class BaseFragment extends Fragment implements BaseView {


    private CatLoadingView loading = new CatLoadingView();

    @Override
    public void showLoading() {
        loading.show(getFragmentManager(), "");
    }

    @Override
    public void hideLoading() {
        if (loading.isAdded())
            loading.dismiss();
    }

    @Override
    public void showError(DefaultErrorBundle errorBundle) {
        Toast.makeText(getActivity(), errorBundle.getErrorMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public Context context() {
        return getActivity();
    }

    @Override
    public void showToast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    protected ActivityModule getActivityModule() {
        return new ActivityModule(getActivity());
    }

    protected ApplicationComponent getApplicationComponent() {
        return ((MainApplication) getActivity().getApplicationContext()).getApplicationComponent();
    }

}
