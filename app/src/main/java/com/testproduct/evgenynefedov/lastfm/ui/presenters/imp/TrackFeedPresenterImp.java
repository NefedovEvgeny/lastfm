package com.testproduct.evgenynefedov.lastfm.ui.presenters.imp;

import android.text.TextUtils;
import android.util.Log;

import com.example.exception.DefaultErrorBundle;
import com.example.interator.DefaultObserver;
import com.example.interator.DetailTrackUseCase;
import com.example.interator.SearchTracksUserCase;
import com.example.models.Track;
import com.example.models.TrackBase;
import com.testproduct.evgenynefedov.lastfm.ui.presenters.TrackFeedPresenter;
import com.testproduct.evgenynefedov.lastfm.ui.view.TrackListView;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class TrackFeedPresenterImp implements TrackFeedPresenter {

    private TrackListView view;


    private SearchTracksUserCase searchTracks;



    @Inject
    public TrackFeedPresenterImp(SearchTracksUserCase searchTracks) {
        this.searchTracks = searchTracks;
    }

    public TrackListView getView() {
        return view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        view = null;
        searchTracks.dispose();
    }

    @Override
    public void bind(TrackListView view) {
        this.view = view;
    }

    @Override
    public void search(String trackName) {
        getView().showLoading();
        searchTracks.execute(new TrackListObserver(), SearchTracksUserCase.Params.byName(trackName));
    }

    @Override
    public void onTrackClick(TrackBase trackBase) {
        if (TextUtils.isEmpty(trackBase.getMbid())) {
            getView().showToast("неудалось загрузить корректные данные");
            return;
        }
        getView().navigateToTrack(trackBase);
    }

    private class TrackListObserver extends DefaultObserver<List<TrackBase>> {

        @Override
        public void onComplete() {
            getView().hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.getStackTrace();
            getView().showError(new DefaultErrorBundle((Exception) e));
        }

        @Override
        public void onNext(List<TrackBase> trackBases) {
            initView(trackBases);
        }
    }

    private void initView(List<TrackBase> trackBases) {
        if (trackBases.size()==0) {
            getView().showToast("ой-ей кажется тут ничего");
        }
        getView().renderTracksList(trackBases);
    }


}
