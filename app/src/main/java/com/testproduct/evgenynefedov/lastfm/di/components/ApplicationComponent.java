package com.testproduct.evgenynefedov.lastfm.di.components;

import android.content.Context;

import com.example.executors.PostExecutionThread;
import com.example.executors.ThreadExecutor;
import com.example.repository.TrackRepository;
import com.testproduct.evgenynefedov.lastfm.ui.activity.BaseActivity;
import com.testproduct.evgenynefedov.lastfm.MainApplication;
import com.testproduct.evgenynefedov.lastfm.di.module.ApplicationModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                ApplicationModel.class
        }
)
public interface ApplicationComponent {
    void inject(MainApplication application);
    void inject(BaseActivity baseActivity);

    Context getApplicationContext();
    ThreadExecutor threadExecutor();
    PostExecutionThread postExecutionThread();

    TrackRepository trackRepository();
}