package com.testproduct.evgenynefedov.lastfm.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.testproduct.evgenynefedov.lastfm.R;
import com.testproduct.evgenynefedov.lastfm.di.HasComponent;
import com.testproduct.evgenynefedov.lastfm.di.components.DaggerTrackComponent;
import com.testproduct.evgenynefedov.lastfm.di.components.TrackComponent;
import com.testproduct.evgenynefedov.lastfm.ui.TrackNavigator;
import com.testproduct.evgenynefedov.lastfm.ui.fragments.TrackDetailFragment;
import com.testproduct.evgenynefedov.lastfm.ui.fragments.TracksListFragment;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class TrackDetailActivity extends BaseActivity implements HasComponent<TrackComponent> {
    @Override
    public TrackComponent getComponent() {
        return component;
    }

    private static String EXTRA_ID = "trackId";
    private TrackComponent component;

    private void initializeInjector() {
        this.component = DaggerTrackComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule()).build();

    }

    public static Intent getLaunchIntent(Context context, String trackId) {
        Intent intent = new Intent(context, TrackDetailActivity.class);
        intent.putExtra(EXTRA_ID, trackId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeInjector();
        setContentView(R.layout.activity_container);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String id = bundle.getString(EXTRA_ID, null);
            if (id == null) finish();
            addFragment(R.id.container, TrackDetailFragment.newInstance(id));
        }
    }
}
