package com.testproduct.evgenynefedov.lastfm.ui.presenters.imp;

import android.util.Log;

import com.example.exception.DefaultErrorBundle;
import com.example.interator.DefaultObserver;
import com.example.interator.DetailTrackUseCase;
import com.example.models.Track;
import com.testproduct.evgenynefedov.lastfm.ui.presenters.TrackDetailPresenter;
import com.testproduct.evgenynefedov.lastfm.ui.view.TrackDetailView;

import javax.inject.Inject;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class TrackDetailPresenterImap implements TrackDetailPresenter {
    private TrackDetailView view;
    private DetailTrackUseCase detailTrackUserCase;

    @Inject
    public TrackDetailPresenterImap(DetailTrackUseCase detailTrackUserCase) {
        this.detailTrackUserCase = detailTrackUserCase;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {
        detailTrackUserCase.dispose();
        view = null;
    }

    @Override
    public void bind(TrackDetailView view) {
        this.view = view;
    }


    private TrackDetailView getView() {
        return view;
    }

    @Override
    public void init(String trackId) {
        getView().showLoading();
        detailTrackUserCase.execute(new TrackDetailObserver(), DetailTrackUseCase.Params.byId(trackId));
    }

    private class TrackDetailObserver extends DefaultObserver<Track> {
        @Override
        public void onComplete() {
            getView().hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            e.getStackTrace();
            getView().showError(new DefaultErrorBundle((Exception) e));
        }

        @Override
        public void onNext(Track track) {
            Log.e("что то пришло","пришло");
            initView(track);

        }
    }

    private void initView(Track track) {
        getView().setAlbumAvatar(track.getAlbum().getImage().get(3));
        getView().setDescription(track.getAlbum().getArtist());
        getView().setListeners("Listeners - "+track.getListeners());
        getView().setPlayCount("Playcount - "+track.getPlaycount());;
        getView().renderTags(track.getTags());
    }
}
