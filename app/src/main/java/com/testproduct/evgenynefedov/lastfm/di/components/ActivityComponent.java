package com.testproduct.evgenynefedov.lastfm.di.components;

import android.app.Activity;

import com.testproduct.evgenynefedov.lastfm.di.module.ActivityModule;
import com.testproduct.evgenynefedov.lastfm.di.scope.PerActivity;

import dagger.Component;
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    //Exposed to sub-graphs.
    Activity activity();


}
