package com.testproduct.evgenynefedov.lastfm.renders;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.models.Image;
import com.example.models.TrackBase;
import com.pedrogomez.renderers.Renderer;
import com.squareup.picasso.Picasso;
import com.testproduct.evgenynefedov.lastfm.R;
import com.testproduct.evgenynefedov.lastfm.ui.OnTrackClickListener;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class TrackFeedRender extends Renderer<TrackBase> {


    @Bind(R.id.tvTrackName)
    TextView trackName;

    @Bind(R.id.ivAvatar)
    ImageView ivAvatar;
    private OnTrackClickListener onTrackClickListener;

    public TrackFeedRender(OnTrackClickListener onTrackClickListener) {
        this.onTrackClickListener = onTrackClickListener;
    }

    @OnClick(R.id.trackContainer)
    void onTrackClickListener() {
        onTrackClickListener.onTrackClick(getContent());
    }

    @Override
    protected void setUpView(View rootView) {

    }

    @Override
    protected void hookListeners(View rootView) {

    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup parent) {
        View inflatedView = inflater.inflate(
                R.layout.item_track, parent, false);
        ButterKnife.bind(this, inflatedView);
        return inflatedView;
    }

    @Override
    public void render() {
        trackName.setText(getContent().getArtist());
        setupAvatar();
    }

    private void setupAvatar() {
        Image firstImage = getContent().getAvatar();
        String imageUrl = firstImage != null ? firstImage.getLink() : null;
        if (!TextUtils.isEmpty(imageUrl))
        Picasso.with(getContext()).load(imageUrl).into(ivAvatar);
    }
}
