package com.testproduct.evgenynefedov.lastfm.di;

/**
 * Created by fold on 14/06/2017.
 */
public interface HasComponent<C> {
    C getComponent();
}
