package com.testproduct.evgenynefedov.lastfm.ui;

import com.example.models.Tag;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public interface OnTagClickListener {
    void onTagClick(Tag tag);
}
