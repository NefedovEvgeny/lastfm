package com.testproduct.evgenynefedov.lastfm.renders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.models.Tag;
import com.pedrogomez.renderers.Renderer;
import com.testproduct.evgenynefedov.lastfm.R;
import com.testproduct.evgenynefedov.lastfm.ui.OnTagClickListener;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class TagsRender extends Renderer<Tag> {
    @Bind(R.id.tvTag)
    TextView tvTag;
    OnTagClickListener onTagClickListener;

    public TagsRender(OnTagClickListener onTagClickListener) {
        this.onTagClickListener = onTagClickListener;
    }

    @Override
    protected void setUpView(View rootView) {

    }

    @Override
    protected void hookListeners(View rootView) {

    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup parent) {
        View inflatedView = inflater.inflate(
                R.layout.item_tag, parent, false);
        ButterKnife.bind(this, inflatedView);
        return inflatedView;
    }

    @OnClick(R.id.tvTag)
    void onTagClickListener(){
        onTagClickListener.onTagClick(getContent());
    }
    @Override
    public void render() {
        tvTag.setText(getContent().getName());
    }
}
