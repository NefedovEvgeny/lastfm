package com.testproduct.evgenynefedov.lastfm.di.components;

import com.testproduct.evgenynefedov.lastfm.di.module.ActivityModule;
import com.testproduct.evgenynefedov.lastfm.di.module.UserModule;
import com.testproduct.evgenynefedov.lastfm.di.scope.PerActivity;
import com.testproduct.evgenynefedov.lastfm.ui.fragments.TrackDetailFragment;
import com.testproduct.evgenynefedov.lastfm.ui.fragments.TracksListFragment;

import dagger.Component;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, UserModule.class})
public interface TrackComponent extends ActivityComponent {
    void inject(TracksListFragment tracksListFragment);

    void inject(TrackDetailFragment trackDetailFragment);
}