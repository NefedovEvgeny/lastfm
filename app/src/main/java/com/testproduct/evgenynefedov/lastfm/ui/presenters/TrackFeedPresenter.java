package com.testproduct.evgenynefedov.lastfm.ui.presenters;

import com.example.models.TrackBase;
import com.testproduct.evgenynefedov.lastfm.ui.view.TrackListView;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public interface TrackFeedPresenter extends Presenter<TrackListView> {
    void search(String trackName);
    void onTrackClick(TrackBase trackBase);
}
