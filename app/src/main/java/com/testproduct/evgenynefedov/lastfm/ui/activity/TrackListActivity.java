package com.testproduct.evgenynefedov.lastfm.ui.activity;

import android.os.Bundle;

import com.example.models.TrackBase;
import com.testproduct.evgenynefedov.lastfm.R;
import com.testproduct.evgenynefedov.lastfm.di.HasComponent;
import com.testproduct.evgenynefedov.lastfm.di.components.DaggerTrackComponent;
import com.testproduct.evgenynefedov.lastfm.di.components.TrackComponent;
import com.testproduct.evgenynefedov.lastfm.ui.TrackNavigator;
import com.testproduct.evgenynefedov.lastfm.ui.fragments.TrackDetailFragment;
import com.testproduct.evgenynefedov.lastfm.ui.fragments.TracksListFragment;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class TrackListActivity extends BaseActivity implements TrackNavigator,HasComponent<TrackComponent> {

    private TrackComponent component;

    private void initializeInjector() {
        this.component = DaggerTrackComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule()).build();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeInjector();
        setContentView(R.layout.activity_container);
        if (savedInstanceState == null) {
            addFragment(R.id.container, TracksListFragment.newInstance());
        }
    }

    @Override
    public TrackComponent getComponent() {
        return component;
    }

    @Override
    public void navigateToTrack(TrackBase trackBase) {
        navigator.navigateToTrackDetail(this, trackBase);
    }
}
