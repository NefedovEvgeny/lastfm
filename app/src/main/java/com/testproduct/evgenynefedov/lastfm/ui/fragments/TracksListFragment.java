package com.testproduct.evgenynefedov.lastfm.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.models.TrackBase;
import com.pedrogomez.renderers.AdapteeCollection;
import com.pedrogomez.renderers.ListAdapteeCollection;
import com.pedrogomez.renderers.RVRendererAdapter;
import com.pedrogomez.renderers.Renderer;
import com.pedrogomez.renderers.RendererBuilder;
import com.testproduct.evgenynefedov.lastfm.R;
import com.testproduct.evgenynefedov.lastfm.di.components.DaggerTrackComponent;
import com.testproduct.evgenynefedov.lastfm.renders.TrackFeedRender;
import com.testproduct.evgenynefedov.lastfm.ui.OnTrackClickListener;
import com.testproduct.evgenynefedov.lastfm.ui.TrackNavigator;
import com.testproduct.evgenynefedov.lastfm.ui.activity.TrackListActivity;
import com.testproduct.evgenynefedov.lastfm.ui.presenters.imp.TrackFeedPresenterImp;
import com.testproduct.evgenynefedov.lastfm.ui.view.TrackListView;

import java.lang.ref.WeakReference;
import java.util.Collection;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class TracksListFragment extends BaseFragment implements TrackListView, OnTrackClickListener {

    private WeakReference<TrackNavigator> trackNavigator;
    private RVRendererAdapter<TrackBase> adapter;

    @Inject
    TrackFeedPresenterImp presenter;

    @Bind(R.id.rlTracks)
    RecyclerView rlTracks;


    public static TracksListFragment newInstance() {

        Bundle args = new Bundle();

        TracksListFragment fragment = new TracksListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setHasOptionsMenu(true);
        if (context instanceof TrackListActivity) {
            this.trackNavigator = new WeakReference<>((TrackListActivity) context);
        } else
            throw new ClassCastException(context.getClass().getName() + " must implements " + OnTrackClickListener.class.getName());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_track_feed, container, false);
        ButterKnife.bind(this, root);
        initInjector();
        presenter.bind(this);
        init();
        return root;
    }

    private void initInjector() {
        DaggerTrackComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build().inject(this);
    }

    @Override
    public void renderTracksList(Collection<TrackBase> trackBases) {
        adapter.clear();
        adapter.addAll(trackBases);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void navigateToTrack(TrackBase trackBase) {
        trackNavigator.get().navigateToTrack(trackBase);
    }

    private void init() {
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        rlTracks.setLayoutManager(manager);
        rlTracks.setHasFixedSize(true);
        adapter = initAdapter();
        rlTracks.setAdapter(adapter);
        loadTracksByName("test");
    }

    private void loadTracksByName(String name) {
        presenter.search(name);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.destroy();
        //TODO  обновить и удерживать ссылку на очистку :)
        ButterKnife.unbind(this);
        trackNavigator.clear();
    }


    private RVRendererAdapter<TrackBase> initAdapter() {
        final AdapteeCollection<TrackBase> videoCollection =
                new ListAdapteeCollection<>();
        Renderer<TrackBase> renderer = new TrackFeedRender(this);
        RendererBuilder<TrackBase> rendererBuilder = new RendererBuilder<>(renderer);
        return new RVRendererAdapter<>(rendererBuilder, videoCollection);
    }

    @Override
    public void onTrackClick(TrackBase trackBase) {
        presenter.onTrackClick(trackBase);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
            inflater.inflate(R.menu.menu_track_feed, menu);
            MenuItem searchItem = menu.findItem(R.id.action_search);
            SearchView searchView =
                    (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(onQueryTextListener);

    }

    SearchView.OnQueryTextListener onQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            loadTracksByName(query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }
    };
}
