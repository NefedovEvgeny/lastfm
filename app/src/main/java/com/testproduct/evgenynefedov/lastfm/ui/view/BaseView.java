package com.testproduct.evgenynefedov.lastfm.ui.view;

import android.content.Context;

import com.example.exception.DefaultErrorBundle;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public interface BaseView {
    //показать загрузку
    void showLoading();

    //скрыть загрузку
    void hideLoading();

    //покзать сообщение об ошибке
    void showError(DefaultErrorBundle errorBundle);

    //дать контекст
    Context context();

    void showToast(String msg);
}
