package com.testproduct.evgenynefedov.lastfm.ui;

import com.example.models.TrackBase;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public interface OnTrackClickListener {
    void onTrackClick(TrackBase trackBase);
}
