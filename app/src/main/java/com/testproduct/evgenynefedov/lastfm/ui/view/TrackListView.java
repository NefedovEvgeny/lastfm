package com.testproduct.evgenynefedov.lastfm.ui.view;

import com.example.models.TrackBase;

import java.util.Collection;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public interface TrackListView extends BaseView {

    void renderTracksList(Collection<TrackBase> trackBases);
    void navigateToTrack(TrackBase trackBase);
}
