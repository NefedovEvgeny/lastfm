package com.testproduct.evgenynefedov.data.repository.datastore;

import android.content.Context;

import com.testproduct.evgenynefedov.data.cache.TrackDetailCache;
import com.testproduct.evgenynefedov.data.di.DaggerWrapper;
import com.testproduct.evgenynefedov.data.model.Output;
import com.testproduct.evgenynefedov.data.model.Results;
import com.testproduct.evgenynefedov.data.model.STrack;
import com.testproduct.evgenynefedov.data.model.STrackDetail;
import com.testproduct.evgenynefedov.data.net.api.TrackApi;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class CloudTrackDataStore implements TrackDataStore {

    @Inject
    TrackApi trackApi;
    private TrackDetailCache trackCache;

    CloudTrackDataStore(Context ctx, TrackDetailCache trackCache) {
        DaggerWrapper.getComponent(ctx).inject(this);
        this.trackCache = trackCache;
    }

    @Override
    public Observable<List<STrack>> getSearchedTrack(String trackName) {
        return trackApi.findTrack(trackName).map
                (results -> results.getResults().getTrackMatches().getTrack());
    }

    @Override
    public Observable<STrackDetail> getTrackDetail(String trackId) {
        return trackApi.getTrack(trackId).map(output -> {
            STrackDetail sTrackDetail = output.getTrack();
            trackCache.put(sTrackDetail);
            return sTrackDetail;
        });
    }


}
