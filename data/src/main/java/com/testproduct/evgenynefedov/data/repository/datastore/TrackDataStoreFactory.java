package com.testproduct.evgenynefedov.data.repository.datastore;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.testproduct.evgenynefedov.data.cache.TrackDetailCache;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class TrackDataStoreFactory {

    private final Context context;
    private TrackDetailCache trackCache;

    @Inject
    TrackDataStoreFactory(@NonNull Context context, @NonNull TrackDetailCache trackCache) {
        this.context = context.getApplicationContext();
        this.trackCache = trackCache;

    }

    public TrackDataStore foundTracks(String track) {
        //TODO пока что списки не поддерживаються
        return new CloudTrackDataStore(this.context, trackCache);
    }

    public TrackDataStore getTrackDetail(String id) {
        if (!this.trackCache.isExpired() && this.trackCache.isCached(id)) {
            Log.e("from", "cache");
            return new DiscTrackDataStore(trackCache);
        } else {
            return new CloudTrackDataStore(this.context, trackCache);
        }
    }

}
