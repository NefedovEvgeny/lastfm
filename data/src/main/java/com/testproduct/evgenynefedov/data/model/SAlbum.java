package com.testproduct.evgenynefedov.data.model;

import com.example.models.Image;

import java.util.List;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class SAlbum {
    List<SImage> image;

    private String artist;
    private String title;
    private String mbid;
    private String url;

    public List<SImage> getImage() {
        return image;
    }

    public String getArtist() {
        return artist;
    }

    public String getTitle() {
        return title;
    }

    public String getMbid() {
        return mbid;
    }

    public String getUrl() {
        return url;
    }
}
