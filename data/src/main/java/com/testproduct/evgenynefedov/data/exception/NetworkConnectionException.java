package com.testproduct.evgenynefedov.data.exception;

/**
 * Created by fold on 09/06/2017.
 */

public class NetworkConnectionException extends Exception {

    public NetworkConnectionException() {
        super();
    }

    public NetworkConnectionException(final Throwable cause) {
        super(cause);
    }
}
