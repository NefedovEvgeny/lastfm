package com.testproduct.evgenynefedov.data.net.api;

import com.testproduct.evgenynefedov.data.model.Output;
import com.testproduct.evgenynefedov.data.model.Results;

import java.util.Observer;
import io.reactivex.Observable;
import io.reactivex.android.BuildConfig;
import retrofit2.http.POST;
import retrofit2.http.Query;

import static com.testproduct.evgenynefedov.data.utils.Credentials.API_KEY;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public interface TrackApi {

    @POST("?method=track.search&api_key="+API_KEY+"&format=json")
    Observable<Output> findTrack(@Query("track") String track);

    @POST("?method=track.getInfo&api_key="+API_KEY+"&format=json")
    Observable<Output> getTrack(@Query("mbid") String trackId);
}
