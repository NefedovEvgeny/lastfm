package com.testproduct.evgenynefedov.data.model;

import java.util.List;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class STrackDetail implements ServerModel {

    private SArtist artist;
    private String mbid;
    private SAlbum album;

    private long listeners;

    private long playcount;
    private STopTags toptags;

    public List<STag> getToptags() {
        return toptags.getTag();
    }

    public SAlbum getAlbum() {
        return album;
    }


    public long getListeners() {
        return listeners;
    }

    public long getPlaycount() {
        return playcount;
    }

    public String getMbid() {
        return mbid;
    }

    public SArtist getArtist() {
        return artist;
    }
}
