package com.testproduct.evgenynefedov.data.cache;

import android.content.Context;

import com.example.executors.ThreadExecutor;
import com.testproduct.evgenynefedov.data.exception.TrackNotFountExaction;
import com.testproduct.evgenynefedov.data.model.ServerModel;

import java.io.File;

import io.reactivex.Observable;

/**
 * Created by fold on 21/06/2017.
 */

public abstract class CacheImap<T extends ServerModel> implements Cache<T> {

    private static final String SETTINGS_FILE_NAME = "com.emanor.roopr.presentation.implementation.announcement.testData.SETTINGS";
    private static final String LAST_UPDATE_KEY_FORMAT = "%s_last_cache_update";

    private final Context context;
    private final File cacheDir;
    private final Serializer serializer;
    private final FileManager fileManager;
    private final ThreadExecutor threadExecutor;

    public CacheImap(Context context, Serializer serializer,
                     FileManager fileManager, ThreadExecutor executor) {
        if (context == null || serializer == null || fileManager == null || executor == null) {
            throw new IllegalArgumentException("Invalid null parameter");
        }
        this.context = context.getApplicationContext();
        this.cacheDir = this.context.getCacheDir();
        this.serializer = serializer;
        this.fileManager = fileManager;
        this.threadExecutor = executor;
    }

    public abstract long getExpirationTime();

    public abstract String getUpdateKey();

    public abstract Class<T> getOperateClass();


    @Override
    public Observable<T> get(String id) {
        return Observable.create(emitter -> {
            final File userEntityFile = CacheImap.this.buildFile(id);
            final String fileContent = CacheImap.this.fileManager.readFileContent(userEntityFile);
            final T userEntity =
                    CacheImap.this.serializer.deserialize(fileContent, getOperateClass());

            if (userEntity != null) {
                emitter.onNext(userEntity);
                emitter.onComplete();
            } else {
                emitter.onError(new TrackNotFountExaction());
            }
        });
    }

    @Override
    public void put(T entity) {
        if (entity != null) {
            final File userEntityFile = this.buildFile(entity.getMbid());
            if (!isCached(entity.getMbid())) {
                final String jsonString = this.serializer.serialize(entity, getOperateClass());
                this.executeAsynchronously(new CacheImap.CacheWriter(this.fileManager, userEntityFile, jsonString));
                setLastCacheUpdateTimeMillis();
            }
        }
    }


    @Override
    public boolean isCached(String id) {
        final File userEntityFile = this.buildFile(id);
        return this.fileManager.exists(userEntityFile);
    }

    @Override
    public boolean isExpired() {
        long currentTime = System.currentTimeMillis();
        long lastUpdateTime = this.getLastCacheUpdateTimeMillis();

        boolean expired = ((currentTime - lastUpdateTime) > getExpirationTime());

        if (expired) {
            this.evictAll();
        }

        return expired;
    }

    @Override
    public void evictAll() {
        this.executeAsynchronously(new CacheImap.CacheEvictor(this.fileManager, this.cacheDir));
    }

    private File buildFile(String userId) {
        final StringBuilder fileNameBuilder = new StringBuilder();
        fileNameBuilder.append(this.cacheDir.getPath());
        fileNameBuilder.append(File.separator);
        fileNameBuilder.append(getOperateClass().getSimpleName());//TODO плохо
        fileNameBuilder.append(userId);

        return new File(fileNameBuilder.toString());
    }

    private void setLastCacheUpdateTimeMillis() {
        String updateKey = String.format(LAST_UPDATE_KEY_FORMAT, getUpdateKey());
        final long currentMillis = System.currentTimeMillis();
        this.fileManager.writeToPreferences(this.context, SETTINGS_FILE_NAME,
                updateKey, currentMillis);
    }

    private long getLastCacheUpdateTimeMillis() {
        String updateKey = String.format(LAST_UPDATE_KEY_FORMAT, getUpdateKey());
        return this.fileManager.getFromPreferences(this.context, SETTINGS_FILE_NAME,
                updateKey);
    }


    private void executeAsynchronously(Runnable runnable) {
        this.threadExecutor.execute(runnable);
    }


    private static class CacheWriter implements Runnable {
        private final FileManager fileManager;
        private final File fileToWrite;
        private final String fileContent;

        CacheWriter(FileManager fileManager, File fileToWrite, String fileContent) {
            this.fileManager = fileManager;
            this.fileToWrite = fileToWrite;
            this.fileContent = fileContent;
        }

        @Override
        public void run() {
            this.fileManager.writeToFile(fileToWrite, fileContent);
        }
    }

    private static class CacheEvictor implements Runnable {
        private final FileManager fileManager;
        private final File cacheDir;

        CacheEvictor(FileManager fileManager, File cacheDir) {
            this.fileManager = fileManager;
            this.cacheDir = cacheDir;
        }

        @Override
        public void run() {
            this.fileManager.clearDirectory(this.cacheDir);
        }
    }
}
