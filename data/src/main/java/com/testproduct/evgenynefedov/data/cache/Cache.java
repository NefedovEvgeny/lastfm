package com.testproduct.evgenynefedov.data.cache;

import io.reactivex.Observable;

public interface Cache<T> {

    Observable<T> get(final String id);

    void put(T serverObject);

    boolean isCached(final String id);

    boolean isExpired();

    void evictAll();
}
