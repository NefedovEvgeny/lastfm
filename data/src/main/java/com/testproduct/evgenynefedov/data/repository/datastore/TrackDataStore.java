package com.testproduct.evgenynefedov.data.repository.datastore;

import com.testproduct.evgenynefedov.data.model.Results;
import com.testproduct.evgenynefedov.data.model.STrack;
import com.testproduct.evgenynefedov.data.model.STrackDetail;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public interface TrackDataStore {
    Observable<List<STrack>> getSearchedTrack(String trackName);
    Observable<STrackDetail> getTrackDetail(String trackId);
}
