package com.testproduct.evgenynefedov.data.model;

import java.util.List;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class STrack implements ServerModel {
    private String name;
    private String artist;
    private List<SImage> image;
    private String mbid;


    public String getMbid() {
        return mbid;
    }

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public List<SImage> getImage() {
        return image;
    }
}
