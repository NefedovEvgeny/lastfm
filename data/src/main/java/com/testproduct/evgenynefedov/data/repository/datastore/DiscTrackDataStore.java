package com.testproduct.evgenynefedov.data.repository.datastore;

import com.testproduct.evgenynefedov.data.cache.TrackDetailCache;
import com.testproduct.evgenynefedov.data.model.STrack;
import com.testproduct.evgenynefedov.data.model.STrackDetail;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class DiscTrackDataStore implements TrackDataStore {
    private TrackDetailCache trackCache;

    public DiscTrackDataStore(TrackDetailCache trackCache) {
        this.trackCache = trackCache;
    }

    @Override
    public Observable<List<STrack>> getSearchedTrack(String trackName) {
        return null;
    }

    @Override
    public Observable<STrackDetail> getTrackDetail(String trackId) {
        return trackCache.get(trackId);
    }
}
