package com.testproduct.evgenynefedov.data.repository;

import com.example.models.Track;
import com.example.models.TrackBase;
import com.example.repository.TrackRepository;
import com.testproduct.evgenynefedov.data.entery.TrackResourceDataMapper;
import com.testproduct.evgenynefedov.data.model.STrack;
import com.testproduct.evgenynefedov.data.model.STrackDetail;
import com.testproduct.evgenynefedov.data.repository.datastore.TrackDataStore;
import com.testproduct.evgenynefedov.data.repository.datastore.TrackDataStoreFactory;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class TrackDataRepository implements TrackRepository {

    TrackDataStoreFactory trackDataStoreFactory;
    TrackResourceDataMapper trackResourceDataMapper;


    private TrackDataStore trackDataStore;
    @Inject
    TrackDataRepository(TrackDataStoreFactory trackDataStoreFactory,
                        TrackResourceDataMapper trackResourceDataMapper) {
        this.trackResourceDataMapper = trackResourceDataMapper;
        this.trackDataStoreFactory = trackDataStoreFactory;
    }

    @Override
    public Observable<List<TrackBase>> searchTracks(String trackName) {
        trackDataStore = trackDataStoreFactory.foundTracks(trackName);
        return trackDataStore.getSearchedTrack(trackName).map(sTracks -> trackResourceDataMapper.mappingTrack(sTracks));
    }

    @Override
    public Observable<Track> getTrackDetail(String mbid) {
        trackDataStore = trackDataStoreFactory.getTrackDetail(mbid);
        return trackDataStore.getTrackDetail(mbid).map(sTrackDetail -> trackResourceDataMapper.mappingTrack(sTrackDetail));
    }
}
