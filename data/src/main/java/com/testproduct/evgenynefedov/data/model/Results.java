package com.testproduct.evgenynefedov.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class Results {
    @SerializedName("trackmatches")
    private TrackMatches trackMatches;

    public TrackMatches getTrackMatches() {
        return trackMatches;
    }
}
