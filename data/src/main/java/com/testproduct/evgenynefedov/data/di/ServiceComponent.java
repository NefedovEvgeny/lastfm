package com.testproduct.evgenynefedov.data.di;

import com.testproduct.evgenynefedov.data.repository.datastore.CloudTrackDataStore;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                NetworkModule.class,
//                Utils.class
        }
)
public interface ServiceComponent {
    void inject(CloudTrackDataStore cloudTrackDataStore);

//    void inject(CloudAnnouncementDataStore cloudUserDataStore);
//    void inject(CloudEventDataScore cloudEventTapeDataScore);
//    void inject(CloudCommunityDataStore cloudCommunityDataStore);
//    void inject(AuthorizationCloudDataScore authorizationCloudDataScore);
//
//    void inject(ServiceGenerator serviceGenerator);
//
//    void inject(CloudUserDataScore cloudContactDataScore);

}
