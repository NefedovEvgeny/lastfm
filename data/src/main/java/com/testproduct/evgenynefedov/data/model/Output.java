package com.testproduct.evgenynefedov.data.model;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class Output {
    Results results;
    STrackDetail track;

    public STrackDetail getTrack() {
        return track;
    }

    public Results getResults() {
        return results;
    }
}
