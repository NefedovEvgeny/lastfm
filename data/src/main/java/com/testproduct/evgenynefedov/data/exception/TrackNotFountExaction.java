package com.testproduct.evgenynefedov.data.exception;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class TrackNotFountExaction extends Exception {
    public TrackNotFountExaction() {
        super();
    }

    public TrackNotFountExaction(final Throwable cause) {
        super(cause);
    }
}
