package com.testproduct.evgenynefedov.data.model;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class SArtist {
    private String name;
    private String url;
    private String mbid;

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getMbid() {
        return mbid;
    }
}
