package com.testproduct.evgenynefedov.data.cache;

import android.content.Context;

import com.example.executors.ThreadExecutor;
import com.testproduct.evgenynefedov.data.model.STrack;
import com.testproduct.evgenynefedov.data.model.STrackDetail;

import javax.inject.Inject;


public class TrackDetailCache extends CacheImap<STrackDetail> {

    private static final long MINUTE = 1000 * 60;
    @Inject
    public TrackDetailCache(Context context, Serializer serializer, FileManager fileManager, ThreadExecutor executor) {
        super(context, serializer, fileManager, executor);
    }

    @Override
    public long getExpirationTime() {
        return MINUTE * 10;
    }

    @Override
    public String getUpdateKey() {
        return getClass().getSimpleName();
    }

    @Override
    public Class<STrackDetail> getOperateClass() {
        return STrackDetail.class;
    }
}
