package com.testproduct.evgenynefedov.data.model;

import java.util.List;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class STopTags {
    private List<STag> tag;

    public List<STag> getTag() {
        return tag;
    }
}
