package com.testproduct.evgenynefedov.data.di;

import android.content.Context;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.testproduct.evgenynefedov.data.BuildConfig;
import com.testproduct.evgenynefedov.data.net.api.TrackApi;

import java.lang.annotation.Annotation;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    private static final int CONNECT_TIMEOUT = 10; // connect timeout
    private static final int SOCKET_TIMEOUT = 10; // socket timeout


    private Context appContext;

    public NetworkModule(Context appContext) {
        this.appContext = appContext;
    }

    @Provides
    Context provideContext() {
        return appContext;
    }

    @Provides
    @Singleton
    OkHttpClient provideClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(logging);
        builder.connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS); // connect timeout
        return builder.build();
    }



    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        //        retrofit.responseBodyConverter(ErrorRootModel.class, new Annotation[0]);
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
//                .addConverterFactory(cf)
                .addConverterFactory(GsonConverterFactory.create())
//                .addConverterFactory(new EnumRetrofitConverterFactory())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }


    @Provides
    @Singleton
    TrackApi provideTrackApi(Retrofit retrofit) {
        return retrofit.create(TrackApi.class);
    }

}
