package com.testproduct.evgenynefedov.data.di;

import android.content.Context;

/**
 * Created by fold on 19/06/2017.
 */

public class DaggerWrapper {
    private static ServiceComponent mComponent;

    public static ServiceComponent getComponent(Context ctx) {
        if (mComponent == null) {
            initComponent(ctx);
        }
        return mComponent;
    }

    private static void initComponent (Context ctx) {
        mComponent = DaggerServiceComponent
                .builder()
                .networkModule(new NetworkModule(ctx))
                .build();
    }
}
