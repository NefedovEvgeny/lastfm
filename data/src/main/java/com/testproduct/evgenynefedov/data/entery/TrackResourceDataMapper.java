package com.testproduct.evgenynefedov.data.entery;

import com.example.models.Album;
import com.example.models.Artist;
import com.example.models.Image;
import com.example.models.Tag;
import com.example.models.Track;
import com.example.models.TrackBase;
import com.testproduct.evgenynefedov.data.model.SAlbum;
import com.testproduct.evgenynefedov.data.model.SArtist;
import com.testproduct.evgenynefedov.data.model.SImage;
import com.testproduct.evgenynefedov.data.model.STag;
import com.testproduct.evgenynefedov.data.model.STrack;
import com.testproduct.evgenynefedov.data.model.STrackDetail;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class TrackResourceDataMapper {


    @Inject
    public TrackResourceDataMapper() {
    }

    public List<TrackBase> mappingTrack(List<STrack> sTracks) {
        List<TrackBase> localModels = new ArrayList<>(sTracks.size());

        for (STrack sTrack : sTracks) {
            TrackBase trackBase = new TrackBase(sTrack.getMbid());
            trackBase.setArtist(sTrack.getArtist());
            trackBase.setName(trackBase.getName());
            trackBase.setImage(mappingImage(sTrack.getImage()));
            localModels.add(trackBase);
        }
        return localModels;
    }

    public Track mappingTrack(STrackDetail sTrackDetail) {
        Track track = new Track(sTrackDetail.getMbid());
        track.setArtist(mappingArtist(sTrackDetail.getArtist()));
        track.setAlbum(mappingAlbum(sTrackDetail.getAlbum()));
        track.setListeners(sTrackDetail.getListeners());
        track.setPlaycount(sTrackDetail.getPlaycount());
        track.setTags(mappingTag(sTrackDetail.getToptags()));
        return track;

    }

    private List<Tag> mappingTag(List<STag> sTagList) {
        if (sTagList==null) return null;
        List<Tag> tagList = new ArrayList<>(sTagList.size());
        for (STag sTag:sTagList) {
            Tag tag = new Tag();
            tag.setUrl(sTag.getUrl());
            tag.setName(sTag.getName());
            tagList.add(tag);
        }
        return tagList;
    }

    private Album mappingAlbum(SAlbum sAlbum) {
        if (sAlbum == null) return null;
        Album album = new Album(sAlbum.getMbid());
        album.setUrl(sAlbum.getUrl());
        album.setArtist(sAlbum.getArtist());
        album.setImage(mappingImage(sAlbum.getImage()));
        album.setTitle(sAlbum.getTitle());
        return album;

    }

    private Artist mappingArtist(SArtist sArtist) {
        if (sArtist == null) return null;
        Artist artist = new Artist(sArtist.getMbid());
        artist.setName(sArtist.getName());
        artist.setUrl(sArtist.getUrl());
        return artist;
    }

    private List<Image> mappingImage(List<SImage> sImages) {
        List<Image> localModels = new ArrayList<>(sImages.size());

        for (SImage sImage : sImages) {
            Image image = new Image();
            image.setLink(sImage.getLink());
            localModels.add(image);
        }
        return localModels;
    }

}
