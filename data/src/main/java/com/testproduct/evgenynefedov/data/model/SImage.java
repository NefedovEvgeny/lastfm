package com.testproduct.evgenynefedov.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class SImage {
    @SerializedName("#text")
    private String link;

    public String getLink() {
        return link;
    }
}
