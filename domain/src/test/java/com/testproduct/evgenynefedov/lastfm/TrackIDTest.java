/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.testproduct.evgenynefedov.lastfm;

import com.example.models.Track;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TrackIDTest {

  private static final String FAKE_ID = "12313";
  private Track track;

  @Before
  public void setUp() {
    track = new Track(FAKE_ID);
  }

  @Test
  public void testTrackConstructorHappyCase() {
    final String id = track.getMbid();
    assertThat(id).isEqualTo(FAKE_ID);
  }
}
