package com.example.executors;

import io.reactivex.Scheduler;

/**
 * Created by fold on 09/06/2017.
 */

public interface PostExecutionThread {
    Scheduler getScheduler();
}