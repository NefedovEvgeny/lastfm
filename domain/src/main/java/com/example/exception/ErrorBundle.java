package com.example.exception;

/**
 * Created by fold on 09/06/2017.
 */

public interface ErrorBundle {
    Exception getException();

    String getErrorMessage();
}
