package com.example.interator;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by fold on 09/06/2017.
 */

public class DefaultObserver<T> extends DisposableObserver<T> {
    @Override
    public void onNext(T t) {
        // no-op by default.
    }

    @Override
    public void onComplete() {
        // no-op by default.
    }

    @Override
    public void onError(Throwable exception) {

    }
}
