package com.example.interator;

import com.example.executors.PostExecutionThread;
import com.example.executors.ThreadExecutor;
import com.example.models.Track;
import com.example.repository.TrackRepository;
import com.fernandocejas.arrow.checks.Preconditions;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class DetailTrackUseCase extends UseCase<Track, DetailTrackUseCase.Params> {


    private TrackRepository repository;

    @Inject
    public DetailTrackUseCase(TrackRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.repository = repository;
    }

    @Override
    public Observable<Track> buildUseCaseObservable(Params params) {
        Preconditions.checkNotNull(params);

        return repository.getTrackDetail(params.trackId);
    }

    public static final class Params {

        private String trackId;

        private Params(String trackId) {
            this.trackId = trackId;

        }

        public static DetailTrackUseCase.Params byId(String trackId) {
            return new DetailTrackUseCase.Params(trackId);
        }
    }

}
