package com.example.interator;

import com.example.executors.PostExecutionThread;
import com.example.executors.ThreadExecutor;
import com.example.models.TrackBase;
import com.example.repository.TrackRepository;
import com.fernandocejas.arrow.checks.Preconditions;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class SearchTracksUserCase extends UseCase<List<TrackBase>, SearchTracksUserCase.Params> {


    private TrackRepository trackRepository;

    @Inject
    protected SearchTracksUserCase(TrackRepository trackRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.trackRepository = trackRepository;
    }

    @Override
    protected Observable<List<TrackBase>> buildUseCaseObservable(Params params) {
        Preconditions.checkNotNull(params);
        return trackRepository.searchTracks(params.trackName);
    }

    public static final class Params {

        private String trackName;

        private Params(String trackName) {
            this.trackName = trackName;

        }
        public static Params byName(String trackName) {
            return new Params(trackName);
        }
    }


}
