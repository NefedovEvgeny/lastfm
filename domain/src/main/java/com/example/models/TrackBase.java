package com.example.models;

import java.util.List;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class TrackBase {

    private String name;
    private String artist;
    private List<Image> image;
    private String mbid;

    public String getMbid() {
        return mbid;
    }

    public TrackBase(String mbid) {
        this.mbid = mbid;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setImage(List<Image> image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public Image getAvatar() {
        if (image==null) return null;
        return this.image.get(3);
    }

    public String getArtist() {
        return artist;
    }
}
