package com.example.models;

import java.util.List;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class Track extends TrackBase {


    private Artist artist;
    private Album album;
    private long listeners;
    private long playcount;
    private List<Tag> tags;

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public long getPlaycount() {
        return playcount;
    }

    public void setPlaycount(long playcount) {
        this.playcount = playcount;
    }

    public void setListeners(long listeners) {
        this.listeners = listeners;
    }

    public long getListeners() {
        return listeners;
    }



    public Track(String mbid) {
        super(mbid);
    }

    public Artist getArtistObject() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }
}
