package com.example.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class Image {
    private String link;

    public void setLink(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }
}
