package com.example.models;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class Artist {

    private String name;
    private String url;
    private String mbid;

    public Artist(String mbid) {
        this.mbid = mbid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getMbid() {
        return mbid;
    }
}
