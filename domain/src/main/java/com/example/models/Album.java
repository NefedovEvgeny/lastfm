package com.example.models;

import java.util.List;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class Album {
    private String artist;
    private String title;
    private String mbid;
    private String url;
    private List<Image> image;

    public Album(String mbid) {
        this.mbid = mbid;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMbid() {
        return mbid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Image> getImage() {
        return image;
    }

    public void setImage(List<Image> image) {
        this.image = image;
    }
}
