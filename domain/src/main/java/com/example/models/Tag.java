package com.example.models;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public class Tag {
    private String name;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
