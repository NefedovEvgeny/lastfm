package com.example.repository;

import com.example.models.Track;
import com.example.models.TrackBase;

import java.util.List;



import io.reactivex.Observable;

/**
 * Created by EvgenyNefedov on 9/2/17.
 */

public interface TrackRepository {
    Observable<List<TrackBase>> searchTracks(String trackName);


    Observable<Track> getTrackDetail(String mbid);

}
